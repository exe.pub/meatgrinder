from fabric.api import *


env.hosts = ['xon.teichisma.info:15122']
env.user = 'root'
local_user = 'xonotic'
local_home = '/home/xonotic'
app_path = '/home/xonotic/meatgrinder'


def compile_app():
    local('cargo build --release')
    local('strip -s target/release/meatgrinder')

def deploy():
    compile_app()
    run('systemctl stop meatgrinder')
    put('target/release/meatgrinder', app_path)
    run('chmod +x {}/meatgrinder'.format(app_path))
    run('systemctl start meatgrinder')
    local('tar czf /tmp/archive.tar.gz static/ migrations/')
    put('/tmp/archive.tar.gz', '/tmp')
    with cd(app_path):
        run('tar xzf /tmp/archive.tar.gz')
