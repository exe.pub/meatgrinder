use crate::constants::*;
use crate::schema::*;
use diesel::prelude::*;
use failure::{bail, Error};
use rand::distributions::{Distribution, Uniform};
use std::cmp::Ordering;

#[derive(Identifiable, Queryable)]
#[table_name = "league"]
pub struct League {
    pub id: i32,
}

impl League {
    pub fn get_all(conn: &diesel::PgConnection) -> Result<Vec<Self>, Error> {
        Ok(league::table.order_by(league::id.asc()).load(conn)?)
    }

    pub fn get_server(&self, conn: &diesel::PgConnection) -> Result<Option<Server>, Error> {
        Ok(server::table
            .filter(server::league_id.eq(self.id))
            .first(conn)
            .optional()?)
    }
    pub fn generate_games(&self, round_id: i32, conn: &diesel::PgConnection) -> Result<(), Error> {
        diesel::insert_into(game::table)
            .values(NewGame {
                league_id: self.id,
                round_id,
            })
            .execute(conn)?;
        Ok(())
    }
}

#[derive(Identifiable, Queryable, Debug, Clone, PartialEq, Eq, Hash)]
#[table_name = "player"]
pub struct Player {
    pub id: i32,
    pub nickname: String,
    pub league_id: Option<i32>,
    pub country: String,
    pub seeding: i32,
}

impl Player {
    pub fn get_by_id(id: i32, conn: &diesel::PgConnection) -> Result<Self, Error> {
        Ok(player::table.filter(player::id.eq(id)).first(conn)?)
    }
    pub fn get_all(conn: &diesel::PgConnection) -> Result<Vec<Self>, Error> {
        Ok(player::table.order_by(player::seeding.asc()).load(conn)?)
    }

    pub fn by_league(
        current_round: i32,
        conn: &diesel::PgConnection,
    ) -> Result<Vec<(i32, Vec<(Self, Option<(GameScore, Game)>)>)>, Error> {
        let players: Vec<(Player, Option<(GameScore, Game)>)> = player::table
            .order_by(player::league_id.asc())
            .left_outer_join(game_score::table.inner_join(game::table))
            .filter(game::round_id.eq(current_round - 1))
            .or_filter(game::round_id.is_null())
            .load(conn)?;
        let mut current_league_id = 1;
        let mut current_league = Vec::new();
        let mut res = Vec::new();
        for (i, gs) in players {
            if i.league_id.is_none() {
                continue;
            }
            if i.league_id.unwrap() == current_league_id {
                current_league.push((i, gs))
            } else {
                res.push((current_league_id, current_league.clone()));
                current_league.clear();
                current_league_id = i.league_id.unwrap();
                current_league.push((i, gs))
            }
        }
        if current_league.len() > 0 {
            res.push((current_league_id, current_league));
        }
        for i in res.iter_mut() {
            i.1.sort_by(|(p1, s1), (p2, s2)| {
                if s1.is_none() || s2.is_none() {
                    p1.seeding.cmp(&p2.seeding)
                } else {
                    let (score1, game1) = s1.as_ref().unwrap();
                    let (score2, game2) = s2.as_ref().unwrap();
                    if game1.league_id != game2.league_id {
                        game1.league_id.cmp(&game2.league_id)
                    } else {
                        score1.cmp(&score2)
                    }
                }
            });
        }
        Ok(res)
    }

    pub fn promote(&self, conn: &diesel::PgConnection) -> Result<(), Error> {
        if let Some(l_id) = self.league_id {
            if l_id > 1 {
                diesel::update(self)
                    .set(player::league_id.eq(Some(l_id - 1)))
                    .execute(conn)?;
            }
        }
        Ok(())
    }

    pub fn relegate(&self, conn: &diesel::PgConnection) -> Result<(), Error> {
        if let Some(l_id) = self.league_id {
            let leagues = League::get_all(conn)?;
            let ubernoobs = leagues.iter().last();
            if l_id < ubernoobs.unwrap().id {
                diesel::update(self)
                    .set(player::league_id.eq(Some(l_id + 1)))
                    .execute(conn)?;
            }
        }
        Ok(())
    }

    pub fn shift_seeds_up(seed_number: i32, conn: &diesel::PgConnection) -> Result<(), Error> {
        diesel::update(player::table.filter(player::seeding.ge(seed_number)))
            .set(player::seeding.eq(player::seeding + 1))
            .execute(conn)?;
        Ok(())
    }

    pub fn fix_seeds(conn: &diesel::PgConnection) -> Result<(), Error> {
        let players = Player::get_all(conn)?;

        for (i, p) in players.iter().enumerate() {
            println!("{:?} {}", p, i);
            let cur_seed = i as i32;
            if p.seeding != cur_seed + 1 {
                diesel::update(player::table.filter(player::id.eq(p.id)))
                    .set(player::seeding.eq(cur_seed + 1))
                    .execute(conn)?;
            }
        }

        Ok(())
    }

    pub fn regen_leagues(conn: &diesel::PgConnection) -> Result<(), Error> {
        let null: Option<i32> = None;
        diesel::update(player::table)
            .set(player::league_id.eq(null))
            .execute(conn)?;
        // diesel::delete(league::table).execute(conn)?;
        let players = Self::get_all(conn)?;
        let total = players.len();
        let league_size = 4;
        let mut league_size_next_to_last = 4;
        let mut league_size_last = 4;
        let total_leagues;
        match total % 4 {
            0 => {
                total_leagues = total / league_size;
            }
            1 => {
                total_leagues = total / league_size;
                league_size_last = 5;
            }
            2 => {
                total_leagues = total / league_size + 1;
                league_size_next_to_last = 3;
                league_size_last = 3;
            }
            _ => {
                // 3
                total_leagues = total / league_size + 1;
                league_size_last = 3;
            }
        }
        let mut current_league: usize = 1;
        let mut current_league_size = 0;
        for p in players.iter() {
            let league_inserted = league::table
                .filter(league::id.eq(current_league as i32))
                .first::<League>(conn)
                .optional()?
                .is_some();
            if !league_inserted {
                diesel::insert_into(league::table)
                    .values(&league::id.eq(current_league as i32))
                    .execute(conn)?;
            }
            diesel::update(player::table.filter(player::id.eq(p.id)))
                .set(player::league_id.eq(current_league as i32))
                .execute(conn)?;
            current_league_size += 1;
            let desired_size = match total_leagues - current_league {
                0 => league_size_last,
                1 => league_size_next_to_last,
                _ => league_size,
            };
            if current_league_size == desired_size {
                current_league += 1;
                current_league_size = 0;
            }
        }
        println!("{}", current_league);
        diesel::delete(server::table.filter(server::league_id.ge(current_league as i32)))
            .execute(conn)?;
        diesel::delete(league::table.filter(league::id.ge(current_league as i32))).execute(conn)?;

        Ok(())
    }
}

#[derive(Queryable)]
struct PlayerSub {
    id: i32,
    player_id: i32,
    country: String,
    nickname: String,
    is_active: bool,
}

#[derive(Identifiable, Queryable)]
#[table_name = "map"]
pub struct Map {
    pub id: i32,
    pub name: String,
}

impl Map {
    pub fn get_all(conn: &diesel::PgConnection) -> Result<Vec<Self>, Error> {
        Ok(map::table.order_by(map::name.asc()).load(conn)?)
    }

    pub fn pick_random_unplayed(conn: &diesel::PgConnection) -> Result<i32, Error> {
        // Proper SQL query select map.id,count(round.map_id) from map left outer join round on map.id = round.map_id group by map.id;
        // but diesel doesn't yet support GROUP BY
        // so doing it in a pervert new_player
        // kaputt
        let maps = map::table.load::<Self>(conn)?;
        let rounds = Round::belonging_to(&maps)
            .load::<Round>(conn)?
            .grouped_by(&maps);
        let data = maps.into_iter().zip(rounds).collect::<Vec<_>>();
        let mut maps_with_minimal_rounds = Vec::new();
        let mut minimal_rounds = None;
        for i in data.iter() {
            let n = i.1.len();
            match minimal_rounds {
                Some(prev_n) => {
                    if prev_n > n {
                        maps_with_minimal_rounds.clear();
                        minimal_rounds = Some(n);
                        maps_with_minimal_rounds.push(i.0.id)
                    } else if prev_n == n {
                        maps_with_minimal_rounds.push(i.0.id)
                    }
                }
                None => {
                    minimal_rounds = Some(n);
                    maps_with_minimal_rounds.push(i.0.id)
                }
            }
        }
        let ids = Uniform::from(0..maps_with_minimal_rounds.len());
        let mut rng = rand::thread_rng();
        Ok(maps_with_minimal_rounds[ids.sample(&mut rng)])
    }
}

#[derive(Identifiable, Queryable, Associations, Insertable, Debug)]
#[belongs_to(Map)]
#[table_name = "round"]
pub struct Round {
    pub id: i32,
    pub map_id: i32,
    pub finished: bool,
}

impl Round {
    pub fn get_map(&self, conn: &diesel::PgConnection) -> Result<Map, Error> {
        Ok(map::table.filter(map::id.eq(self.map_id)).first(conn)?)
    }

    pub fn get_all(conn: &diesel::PgConnection) -> Result<Vec<Self>, Error> {
        Ok(round::table.order_by(round::id.asc()).load(conn)?)
    }

    pub fn get_latest(conn: &diesel::PgConnection) -> Result<Self, Error> {
        Ok(round::table.order_by(round::id.desc()).first(conn)?)
    }

    pub fn generate(map_id: i32, conn: &diesel::PgConnection) -> Result<i32, Error> {
        let current_total = Self::get_all(conn)?.len() as i32;
        if current_total >= NUMBER_OF_ROUNDS {
            bail!("Too many rounds")
        }
        diesel::insert_into(round::table)
            .values(Round {
                id: current_total + 1,
                map_id,
                finished: false,
            })
            .execute(conn)?;
        Ok(current_total + 1)
    }

    pub fn purge(conn: &diesel::PgConnection) -> Result<(), Error> {
        diesel::delete(game_score::table).execute(conn)?;
        diesel::delete(game::table).execute(conn)?;
        diesel::delete(round::table).execute(conn)?;
        Ok(())
    }

    pub fn all_scores_entered(&self, conn: &diesel::PgConnection) -> Result<bool, Error> {
        let games = Game::games_for_round(self.id, conn)?;
        let mut have_everything = true;
        for g in games.iter() {
            let players = g.get_players(conn)?;
            for p in players.iter() {
                let score = g.get_score_for_player(p.id, conn)?;
                if score.is_none() {
                    have_everything = false;
                }
            }
        }
        Ok(have_everything)
    }

    pub fn revert_round(&self, conn: &diesel::PgConnection) -> Result<(), Error> {
        let games = Game::games_for_round(self.id, conn)?;
        for g in games.iter() {
            diesel::delete(game_score::table.filter(game_score::game_id.eq(g.id))).execute(conn)?;
            diesel::delete(game::table.filter(game::id.eq(g.id))).execute(conn)?;
        }
        diesel::delete(round::table.filter(round::id.eq(self.id))).execute(conn)?;
        if self.id > 1 {
            // set leagues from as the were during the previous round
            let previous_round = Round::get_latest(conn)?;
            let games = Game::games_for_round(previous_round.id, conn)?;
            for g in games.iter() {
                let scores = GameScore::belonging_to(g).load::<GameScore>(conn)?;
                for score in scores.iter() {
                    diesel::update(player::table.filter(player::id.eq(score.player_id)))
                        .set(player::league_id.eq(g.league_id))
                        .execute(conn)?;
                }
            }
        } else {
            // regenerate leagues according to seeding numbers
            Player::regen_leagues(conn)?;
        }
        Ok(())
    }

    pub fn set_unfinished(&self, conn: &diesel::PgConnection) -> Result<(), Error> {
        diesel::update(self)
            .set(round::finished.eq(false))
            .execute(conn)?;
        Ok(())
    }

    pub fn set_finished(&self, conn: &diesel::PgConnection) -> Result<(), Error> {
        diesel::update(self)
            .set(round::finished.eq(true))
            .execute(conn)?;
        Ok(())
    }
}

#[derive(Identifiable, Queryable, Associations, Clone, Debug)]
#[belongs_to(Round)]
#[belongs_to(League)]
#[table_name = "game"]
pub struct Game {
    pub id: i32,
    pub round_id: i32,
    pub league_id: i32,
}

impl Game {
    pub fn get(id: i32, conn: &diesel::PgConnection) -> Result<Self, Error> {
        Ok(game::table.filter(game::id.eq(id)).first(conn)?)
    }
    pub fn games_for_round(round_id: i32, conn: &diesel::PgConnection) -> Result<Vec<Self>, Error> {
        Ok(game::table
            .filter(game::round_id.eq(round_id))
            .order_by(game::league_id)
            .load(conn)?)
    }

    pub fn get_players(&self, conn: &diesel::PgConnection) -> Result<Vec<Player>, Error> {
        let players = player::table
            .filter(player::league_id.eq(self.league_id))
            .get_results(conn)?;
        Ok(players)
    }

    pub fn get_score_for_player(
        &self,
        player_id: i32,
        conn: &diesel::PgConnection,
    ) -> Result<Option<GameScore>, Error> {
        let player = player::table.find(player_id).get_result::<Player>(conn)?;
        let score = GameScore::belonging_to(&player)
            .filter(game_score::game_id.eq(self.id))
            .first::<GameScore>(conn)
            .optional()?;
        Ok(score)
    }

    pub fn get_promoted_and_relegated(
        &self,
        conn: &diesel::PgConnection,
    ) -> Result<(Vec<Player>, Vec<Player>), Error> {
        let mut promoted = Vec::new();
        let mut relegated = Vec::new();
        let mut scores: Vec<GameScore> = GameScore::belonging_to(self).load(conn)?;
        scores.sort();
        promoted.push(Player::get_by_id(scores[0].player_id, conn)?);
        relegated.push(Player::get_by_id(scores[scores.len() - 1].player_id, conn)?);
        Ok((promoted, relegated))
    }
}

#[derive(Insertable)]
#[table_name = "game"]
struct NewGame {
    round_id: i32,
    league_id: i32,
}

#[derive(Queryable, Identifiable, Associations, PartialEq, Eq, Debug, Clone)]
#[belongs_to(Game)]
#[belongs_to(Player)]
#[table_name = "game_score"]
pub struct GameScore {
    pub id: i32,
    pub game_id: i32,
    pub player_id: i32,
    pub score: i32,
    pub tiebreaker: Option<i32>,
}

impl GameScore {
    pub fn get_totals(
        db_conn: &diesel::PgConnection,
    ) -> Result<Vec<(GameScore, Game, Player)>, Error> {
        let query = game_score::table
            .inner_join(game::table)
            .inner_join(player::table);
        Ok(query.load(db_conn)?)
    }
}

impl Ord for GameScore {
    // Ordering::Less means self won against other
    fn cmp(&self, other: &GameScore) -> Ordering {
        if self.score == other.score {
            if self.tiebreaker == other.tiebreaker {
                Ordering::Equal
            } else {
                if self.tiebreaker.is_none() {
                    Ordering::Greater
                } else if other.tiebreaker.is_none() {
                    Ordering::Less
                } else {
                    other.tiebreaker.unwrap().cmp(&self.tiebreaker.unwrap())
                }
            }
        } else {
            other.score.cmp(&self.score)
        }
    }
}

impl PartialOrd for GameScore {
    fn partial_cmp(&self, other: &GameScore) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[derive(Debug)]
pub enum TournamentState {
    Seeding,
    RoundActive(Round),
    TournamentFinished(Round),
}

impl TournamentState {
    pub fn is_seeding(&self) -> bool {
        match self {
            TournamentState::Seeding => true,
            _ => false,
        }
    }

    pub fn is_round_active(&self) -> bool {
        match self {
            TournamentState::RoundActive(_) => true,
            _ => false,
        }
    }

    pub fn is_tournament_finished(&self) -> bool {
        match self {
            TournamentState::TournamentFinished(_) => true,
            _ => false,
        }
    }
}

pub fn get_tournament_state(conn: &diesel::PgConnection) -> Result<TournamentState, Error> {
    // No rounds -> seeding
    // latest round has pending games -> RoundActive
    // all rounds completed -> TournamentFinished
    let rounds = round::table.limit(1).load::<Round>(conn)?;
    if rounds.len() == 0 {
        Ok(TournamentState::Seeding)
    } else {
        let latest_round = Round::get_latest(conn)?;
        if latest_round.finished && (latest_round.id >= NUMBER_OF_ROUNDS) {
            Ok(TournamentState::TournamentFinished(latest_round))
        } else {
            Ok(TournamentState::RoundActive(latest_round))
        }
    }
}

#[derive(Identifiable, Queryable, Debug)]
#[table_name = "page"]
pub struct Page {
    pub id: i32,
    pub slug: String,
    pub content: String,
}

impl Page {
    pub fn by_slug(slug: &str, conn: &diesel::PgConnection) -> Result<Option<Self>, Error> {
        let page = page::table
            .filter(page::slug.eq(slug))
            .first(conn)
            .optional()?;
        Ok(page)
    }

    pub fn get_all(conn: &diesel::PgConnection) -> Result<Vec<Self>, Error> {
        Ok(page::table.order_by(page::slug).load(conn)?)
    }
}

#[derive(Identifiable, Queryable, Debug, Associations)]
#[belongs_to(League)]
#[table_name = "server"]
pub struct Server {
    pub id: i32,
    pub league_id: i32,
    pub name: String,
    pub address: String,
}

impl Server {
    pub fn get_for_league(
        league_id: i32,
        conn: &diesel::PgConnection,
    ) -> Result<Option<Server>, Error> {
        Ok(server::table
            .filter(server::league_id.eq(league_id))
            .first(conn)
            .optional()?)
    }
}
