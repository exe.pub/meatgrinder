table! {
    game (id) {
        id -> Int4,
        round_id -> Int4,
        league_id -> Int4,
    }
}

table! {
    game_score (id) {
        id -> Int4,
        game_id -> Int4,
        player_id -> Int4,
        score -> Int4,
        tiebreaker -> Nullable<Int4>,
    }
}

table! {
    league (id) {
        id -> Int4,
    }
}

table! {
    map (id) {
        id -> Int4,
        name -> Varchar,
    }
}

table! {
    page (id) {
        id -> Int4,
        slug -> Varchar,
        content -> Text,
    }
}

table! {
    player (id) {
        id -> Int4,
        nickname -> Varchar,
        league_id -> Nullable<Int4>,
        country -> Varchar,
        seeding -> Int4,
    }
}

table! {
    player_sub (id) {
        id -> Int4,
        player_id -> Int4,
        nickname -> Varchar,
        is_active -> Bool,
        country -> Nullable<Varchar>,
    }
}

table! {
    round (id) {
        id -> Int4,
        map_id -> Int4,
        finished -> Bool,
    }
}

table! {
    server (id) {
        id -> Int4,
        league_id -> Int4,
        name -> Varchar,
        address -> Varchar,
    }
}

joinable!(game -> league (league_id));
joinable!(game -> round (round_id));
joinable!(game_score -> game (game_id));
joinable!(game_score -> player (player_id));
joinable!(player -> league (league_id));
joinable!(player_sub -> player (player_id));
joinable!(round -> map (map_id));
joinable!(server -> league (league_id));

allow_tables_to_appear_in_same_query!(
    game,
    game_score,
    league,
    map,
    page,
    player,
    player_sub,
    round,
    server,
);
