#![feature(plugin)]
#![feature(proc_macro_hygiene, decl_macro)]
#![feature(custom_attribute)]

#[macro_use]
extern crate diesel;

#[macro_use]
extern crate diesel_migrations;

#[macro_use]
extern crate rocket_contrib;

mod admin;
mod constants;
mod forms;
mod formula;
mod markup;
mod models;
mod public;
mod schema;

use rocket::fairing::AdHoc;
use rocket::response::NamedFile;
use rocket::{get, ignite, routes};
use std::path::{Path, PathBuf};

#[database("main_db")]
pub struct MainDbConn(diesel::PgConnection);

#[get("/static/<file..>")]
fn files(file: PathBuf) -> Option<NamedFile> {
    NamedFile::open(Path::new("static/").join(file)).ok()
}

embed_migrations!("./migrations");

fn main() {
    ignite()
        .attach(MainDbConn::fairing())
        .attach(AdHoc::on_attach("Database Migrations", |rocket| {
            let conn = MainDbConn::get_one(&rocket).expect("database connection");
            println!("Running migrations");
            match embedded_migrations::run_with_output(&*conn, &mut std::io::stdout()) {
                Ok(()) => {
                    println!("Successfully completed migrations");
                    Ok(rocket)
                }
                Err(_) => {
                    println!("Error running migrations");
                    Err(rocket)
                }
            }
        }))
        .mount(
            "/",
            routes![
                files,
                public::index,
                public::public_page,
                public::servers,
                admin::index,
                admin::new_player,
                admin::edit_player,
                admin::delete_player,
                admin::new_map,
                admin::edit_map,
                admin::delete_map,
                admin::reopen_seeding,
                admin::start_new_round,
                admin::save_game_score,
                admin::finish_round,
                admin::revert_round,
                admin::edit_player_restricted,
                admin::edit_player_restricted_process,
                admin::pages_list,
                admin::new_page,
                admin::edit_page,
                admin::do_save_page,
                admin::server_list,
                admin::server_save
            ],
        )
        .launch();
}
