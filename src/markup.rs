use crate::models;
use dpcolors::DPString;
use maud::{html, Markup, PreEscaped, DOCTYPE};

// TODO: make it a trait
pub fn opt_to_value(o: Option<i32>) -> String {
    match o {
        Some(i) => i.to_string(),
        None => "".into(),
    }
}

pub fn form_field_hidden(name: &str, value: &str) -> Markup {
    let id = format!("form_field_{}", name);
    html! (
        input type="hidden" id=(id) name=(name) value=(value);
    )
}

pub fn form_field(name: &str, label: &str, value: &str) -> Markup {
    let id = format!("form_field_{}", name);
    html! (
        label for=(id) class="sr-only" {
            (label)
        };
        input class="form-control mb-2 mr-2" id=(id) name=(name) value=(value) placeholder=(label);
    )
}

fn head() -> Markup {
    html! {
        head {
            meta charset="utf-8";
            meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no";
            title { "XQC #90. InstaGib FFA Cup" }
            link rel="stylesheet" type="text/css" href="/static/css/bootstrap.min.css";
            link rel="stylesheet" type="text/css" href="/static/css/flag-icon.min.css";
            link rel="stylesheet" type="text/css" href="/static/css/fonts.css";
            link rel="stylesheet" type="text/css" href="/static/css/font-awesome.css";
        }
    }
}

pub fn page(content: Markup) -> Markup {
    html! {
        (DOCTYPE)
        html {
            (head())
            body {
                div class="container" {
                    (content)
                }
                (footer())
            }
        }
    }
}

pub fn page_fluid(content: Markup) -> Markup {
    html! {
        (DOCTYPE)
        html {
            (head())
            body {
                div class="container-fluid" {
                    (content)
                }
                (footer())
            }
        }
    }
}

pub fn player(player: &models::Player) -> Markup {
    let maybe_nickname = DPString::parse(&player.nickname);
    let nickname = match maybe_nickname {
        Ok(dp_string) => html! {
            (PreEscaped(dp_string.to_html()))
        },
        Err(_) => html! { "player.nickname" },
    };
    html! {
        span title=(&player.country) class=(format!("mr-2 flag-icon flag-icon-{}", &player.country)) {
            ""
        }
        (nickname)
    }
}

fn footer() -> Markup {
    html! {
        hr;
        footer class="container mb-3" {
            div class="row" {
                div class="col-12 text-center" {
                    "Made with some help of the "
                        a href="https://rust-lang.org" {"Static Typechecking Power"}
                }
            }
        }
    }
}
