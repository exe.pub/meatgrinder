use rocket::request::{Form, FormError};
use rocket::response::Redirect;
use rocket::{get, post, uri};

use maud::{html, Markup};

use crate::constants::*;
use crate::forms;
use crate::markup::*;
use crate::models;
use crate::models::{get_tournament_state, TournamentState};
use crate::MainDbConn;
use failure::{bail, Error};

fn admin_header() -> Markup {
    html! {
        h1 { "MeatGrinder Admin" }
        hr;
        a href=(uri!(index)) { "Admin Home" }
        span { " | " }
        a href=(uri!(pages_list)) { "Pages" }
        span { " | " }
        a href=(uri!(server_list)) { "Servers"}
        span { " | " }
        a href="/" { "Public Site" }
        hr;

    }
}

#[get("/admin")]
pub fn index(db_conn: MainDbConn) -> Result<Markup, Error> {
    let state = get_tournament_state(&db_conn)?;
    match state {
        TournamentState::Seeding => {
            let players = models::Player::get_all(&db_conn)?;
            let maps = models::Map::get_all(&db_conn)?;
            Ok(page(html! {
                (admin_header())
                h2 { "Seeding Stage" };
                h3 { "Players" };
                @for player in players {
                    div class="row" {
                        div class="col-2" {
                            "League " (player.league_id.unwrap_or(0))
                        }
                        div class="col-8" {
                            form method="post" action=(uri!(edit_player: player_id = player.id)) {
                                div class="form-row" {
                                    div class="col-2" {
                                        (form_field("country", "Country", &player.country));
                                    }
                                    div class="col-6" {
                                        (form_field("nickname", "Nickname", &player.nickname));
                                    }
                                    div class="col-2" {
                                        (form_field("seeding", "Seeding", &player.seeding.to_string()));
                                    }
                                    div class="col-2" {
                                        button class="btn btn-primary" { "Save" };
                                    }
                                }

                            }
                        }
                        div class="col-2" {
                            form method="post" action=(uri!(delete_player: player_id = player.id)) {
                                button class="btn btn-danger" { "Delete" };
                            }
                        }
                    }
                }
                h3 { "Add New Player" };
                form method="post" action=(uri!(new_player)) {
                    div class="form-row" {
                        div class="col-2" {
                            (form_field("country", "Country", ""));
                        }
                        div class="col-6" {
                            (form_field("nickname", "Nickname", ""));
                        }
                        div class="col-2" {
                            (form_field("seeding", "Seeding", "0"));
                        }
                        div class="col-2" {
                            button class="btn btn-primary" { "Submit" };
                        }
                    }

                }
                h3 { "Maps" }
                @for m in maps {
                    div class="row" {
                        div class="col-9" {
                            form method="post" action=(uri!(edit_map: map_id = m.id)) {
                                div class="form-row" {
                                    div class="col-9" {
                                        (form_field("name", "Map Name", &m.name));
                                    }
                                    div class="col-3" {
                                        button class="btn btn-primary" { "Submit" };
                                    }
                                }
                            }
                        }
                        div class="col-3" {
                            form method="post" action=(uri!(delete_map: map_id = m.id)) {
                                button class="btn btn-danger" { "Delete" };
                            }
                        }
                    }
                }
                h3 { "Add new map" }
                form method="post" action=(uri!(new_map)) {
                    div class="form-row" {
                        div class="col-9" {
                            (form_field("name", "Map Name", ""));
                        }
                        div class="col-3" {
                            button class="btn btn-primary" {"Submit"}
                        }
                    }
                }
                hr;
                form method="post" action=(uri!(start_new_round)) {
                    button class="btn btn-success" { "Start the tournament" }
                }
            }))
        }
        TournamentState::RoundActive(round) => {
            let games = models::Game::games_for_round(round.id, &db_conn)?;
            let can_be_finished = round.all_scores_entered(&db_conn)?;
            Ok(page(html! {
                (admin_header())
                h2 {"Round " (round.id) " of " (NUMBER_OF_ROUNDS) }
                h3 {"Map " (round.get_map(&db_conn)?.name)}
                @for g in games {
                    h3 {"Game" (g.league_id) }

                    @for p in g.get_players(&db_conn)?.iter() {
                        div class="row" {
                            div class="col-4" {
                                a class="btn float-left btn-primary btn-sm mr-1" href=(uri!(edit_player_restricted: p.id)) {"edit"}
                                (player(p))

                            }
                            div class="col-8" {
                                form class="form-inline" method="post" action=(uri!(save_game_score)) {
                                    (form_field_hidden("player_id", &p.id.to_string()));
                                    (form_field_hidden("game_id", &g.id.to_string()));
                                    @match g.get_score_for_player(p.id, &db_conn)? {
                                        Some(gs) => {
                                            (form_field("score", "Score", &gs.score.to_string()));
                                            (form_field("tiebreaker", "Tiebreaker", &opt_to_value(gs.tiebreaker)));
                                        },
                                        None => {
                                            (form_field("score", "Score", ""));
                                            (form_field("tiebreaker", "Tiebreaker", ""));
                                        }
                                    }
                                    button class="btn btn-primary mb-2" {"Save"}
                                }
                            }
                        }
                    }
                    hr;
                }
                @if can_be_finished {
                    form method="post" action=(uri!(finish_round)) {
                        button class="btn btn-success" {
                            @if round.id == NUMBER_OF_ROUNDS {
                                "We played enough"
                            }
                            @else {
                                "Start new round"
                            }

                        }
                    }
                }
                hr;
                hr;
                hr;
                form method="post" action=(uri!(revert_round)) {
                    button class="btn btn-danger" {"REVERT Round. Removes all scores entered for the current round"}
                }

            }))
        }
        TournamentState::TournamentFinished(_) => Ok(page(html! {
            (admin_header())
            h2 {"Tournament finished"}
            hr;
            hr;
            hr;
            form method="post" action=(uri!(revert_round)) {
                button class="btn btn-danger" {"Reopen the last round."}
            }
        })),
    }
}

#[post("/admin/new-player", data = "<player>")]
pub fn new_player(
    db_conn: MainDbConn,
    player: Result<Form<forms::Player>, FormError>,
) -> Result<Redirect, Error> {
    let state = get_tournament_state(&db_conn)?;
    if !state.is_seeding() {
        bail!("Can edit players only in Seeding stage")
    }
    match player {
        Ok(p) => {
            println!("{:?}", p);
            p.save_new(&db_conn)?;
            Ok(Redirect::to(uri!(index)))
        }
        Err(e) => {
            println!("{:?}", e);
            bail!("{:?}", e)
        }
    }
}

#[post("/admin/edit-player/<player_id>", data = "<player>")]
pub fn edit_player(
    db_conn: MainDbConn,
    player_id: i32,
    player: Result<Form<forms::Player>, FormError>,
) -> Result<Redirect, Error> {
    let state = get_tournament_state(&db_conn)?;
    if !state.is_seeding() {
        bail!("Can edit players only in Seeding stage")
    }
    match player {
        Ok(p) => {
            println!("{:?}", p);
            p.save_existing(player_id, &db_conn)?;
            Ok(Redirect::to(uri!(index)))
        }
        Err(e) => {
            println!("{:?}", e);
            bail!("{:?}", e)
        }
    }
}

#[post("/admin/delete-player/<player_id>")]
pub fn delete_player(db_conn: MainDbConn, player_id: i32) -> Result<Redirect, Error> {
    let state = get_tournament_state(&db_conn)?;
    if !state.is_seeding() {
        bail!("Can edit players only in Seeding stage")
    }
    forms::Player::delete(player_id, &db_conn)?;
    Ok(Redirect::to(uri!(index)))
}

#[post("/admin/new-map", data = "<map>")]
pub fn new_map(
    db_conn: MainDbConn,
    map: Result<Form<forms::Map>, FormError>,
) -> Result<Redirect, Error> {
    let state = get_tournament_state(&db_conn)?;
    if !state.is_seeding() {
        bail!("Can edit maps only in Seeding stage")
    }
    match map {
        Ok(m) => {
            println!("{:?}", m);
            m.save_new(&db_conn)?;
            Ok(Redirect::to(uri!(index)))
        }
        Err(e) => {
            println!("{:?}", e);
            bail!("{:?}", e)
        }
    }
}

#[post("/admin/new-map/<map_id>", data = "<map>")]
pub fn edit_map(
    db_conn: MainDbConn,
    map_id: i32,
    map: Result<Form<forms::Map>, FormError>,
) -> Result<Redirect, Error> {
    let state = get_tournament_state(&db_conn)?;
    if !state.is_seeding() {
        bail!("Can edit maps only in Seeding stage")
    }
    match map {
        Ok(m) => {
            println!("{:?}", m);
            m.save_existing(map_id, &db_conn)?;
            Ok(Redirect::to(uri!(index)))
        }
        Err(e) => {
            println!("{:?}", e);
            bail!("{:?}", e)
        }
    }
}

#[post("/admin/delete-map/<map_id>")]
pub fn delete_map(db_conn: MainDbConn, map_id: i32) -> Result<Redirect, Error> {
    let state = get_tournament_state(&db_conn)?;
    if !state.is_seeding() {
        bail!("Can edit maps only in Seeding stage")
    }
    forms::Map::delete(map_id, &db_conn)?;
    Ok(Redirect::to(uri!(index)))
}

#[post("/admin/reopen-seeding")]
pub fn reopen_seeding(db_conn: MainDbConn) -> Result<Redirect, Error> {
    let state = get_tournament_state(&db_conn)?;
    if (!state.is_round_active()) || (models::Round::get_all(&db_conn)?.len() > 1) {
        bail!("Can reopen seeding only during the 1st active round")
    }
    models::Round::purge(&db_conn)?;
    Ok(Redirect::to(uri!(index)))
}

#[post("/admin/start-new-round")]
pub fn start_new_round(db_conn: MainDbConn) -> Result<Redirect, Error> {
    let state = get_tournament_state(&db_conn)?;
    if !state.is_seeding() {
        bail!("Can start the new round only during Seeding stage")
    }
    let map_id = models::Map::pick_random_unplayed(&db_conn)?;
    let round_id = models::Round::generate(map_id, &db_conn)?;
    for i in models::League::get_all(&db_conn)?.iter() {
        i.generate_games(round_id, &db_conn)?;
    }
    Ok(Redirect::to(uri!(index)))
}

#[post("/admin/save-game-score", data = "<game_score>")]
pub fn save_game_score(
    db_conn: MainDbConn,
    game_score: Result<Form<forms::GameScore>, FormError>,
) -> Result<Redirect, Error> {
    // TODO: validate tournament state and that the game score belongs to the active round
    match game_score {
        Ok(gs) => {
            println!("{:?}", gs);
            gs.save(&db_conn)?;
        }
        Err(e) => {
            println!("{:?}", e);
            bail!("{:?}", e);
        }
    }
    Ok(Redirect::to(uri!(index)))
}

#[post("/admin/finish-round")]
pub fn finish_round(db_conn: MainDbConn) -> Result<Redirect, Error> {
    let state = get_tournament_state(&db_conn)?;
    match state {
        TournamentState::RoundActive(round) => {
            if !round.all_scores_entered(&db_conn)? {
                bail!("Not all scores entered, please double check")
            }
            round.set_finished(&db_conn)?;
            if round.id == NUMBER_OF_ROUNDS {
                // tournament finished, do we need to do anything?
            } else {
                // update the leagues
                let games = models::Game::games_for_round(round.id, &db_conn)?;
                for g in games.iter() {
                    let (promoted, relegated) = g.get_promoted_and_relegated(&db_conn)?;
                    for p in promoted {
                        p.promote(&db_conn)?;
                    }
                    for p in relegated {
                        p.relegate(&db_conn)?;
                    }
                }
                // generate a new round
                let map_id = models::Map::pick_random_unplayed(&db_conn)?;
                let round_id = models::Round::generate(map_id, &db_conn)?;
                for i in models::League::get_all(&db_conn)?.iter() {
                    i.generate_games(round_id, &db_conn)?;
                }
            }
            Ok(Redirect::to(uri!(index)))
        }
        _ => bail!("Can't finish round when no round is active"),
    }
}

#[post("/admin/revert-round")]
pub fn revert_round(db_conn: MainDbConn) -> Result<Redirect, Error> {
    let state = get_tournament_state(&db_conn)?;
    match state {
        TournamentState::RoundActive(round) => {
            round.revert_round(&db_conn)?;
            Ok(Redirect::to(uri!(index)))
        }
        TournamentState::TournamentFinished(round) => {
            round.set_unfinished(&db_conn)?;
            Ok(Redirect::to(uri!(index)))
        }
        _ => bail!("Can't revert a round when no round has yet started"),
    }
}

#[get("/admin/edit-player-restricted/<player_id>")]
pub fn edit_player_restricted(player_id: i32, db_conn: MainDbConn) -> Result<Markup, Error> {
    let p = models::Player::get_by_id(player_id, &db_conn)?;
    Ok(page(html! {
        h3 {"Editing player " (player(&p))}
        form method="post" action=(uri!(edit_player_restricted: player_id = p.id)) {
            div class="form-row" {
                div class="col-2" {
                    (form_field("country", "Country", &p.country));
                }
                div class="col-6" {
                    (form_field("nickname", "Nickname", &p.nickname));
                }
                div class="col-2" {
                    button class="btn btn-primary" { "Save" };
                }
            }
        }
    }))
}

#[post("/admin/edit-player-restricted/<player_id>", data = "<player>")]
pub fn edit_player_restricted_process(
    db_conn: MainDbConn,
    player_id: i32,
    player: Result<Form<forms::PlayerRestricted>, FormError>,
) -> Result<Redirect, Error> {
    match player {
        Ok(p) => {
            println!("{:?}", p);
            p.save(player_id, &db_conn)?;
            Ok(Redirect::to(uri!(index)))
        }
        Err(e) => {
            println!("{:?}", e);
            bail!("{:?}", e)
        }
    }
}

#[get("/admin/pages")]
pub fn pages_list(db_conn: MainDbConn) -> Result<Markup, Error> {
    let pages = models::Page::get_all(&db_conn)?;
    Ok(page(html! {
        (admin_header())
        h2 {"Pages"}
        ul {
            @for p in pages.iter() {
                li {
                    a href=(uri!(edit_page: page_slug=&p.slug)) { (p.slug) }
                }
            }
        }
        a href=(uri!(new_page)) {"Add new page"}
    }))
}

#[get("/admin/page/new")]
pub fn new_page(db_conn: MainDbConn) -> Result<Markup, Error> {
    Ok(page(html! {
        (admin_header());
        h2 {"New Page"}
        form method="post" action=(uri!(do_save_page)) {
            (form_field("slug", "Slug", ""));
            textarea name="content" class="form-control mb-2" rows="25" placeholder="content" {""}
            button class="btn btn-primary" { "Save" }
        }

    }))
}

#[get("/admin/page/edit/<page_slug>")]
pub fn edit_page(page_slug: String, db_conn: MainDbConn) -> Result<Markup, Error> {
    let maybe_page = models::Page::by_slug(&page_slug, &db_conn)?;
    match maybe_page {
        Some(p) => Ok(page(html! {
            (admin_header());
            h2 {"Edit " (p.slug)}
            form method="post" action=(uri!(do_save_page)) {
                (form_field_hidden("id", &p.id.to_string()));
                (form_field("slug", "Slug", &p.slug));
                textarea name="content" class="form-control mb-2" rows="25" placeholder="content" {(p.content)}
                button class="btn btn-primary" { "Save" }
            }
        })),
        None => bail!("No such page"),
    }
}

#[post("/admin/page/save", data = "<page>")]
pub fn do_save_page(
    page: Result<Form<forms::Page>, FormError>,
    db_conn: MainDbConn,
) -> Result<Redirect, Error> {
    match page {
        Ok(p) => {
            println!("{:?}", p);
            p.save(&db_conn)?;
            Ok(Redirect::to(uri!(pages_list)))
        }
        Err(e) => {
            println!("{:?}", e);
            bail!("{:?}", e)
        }
    }
}

#[get("/admin/servers")]
pub fn server_list(db_conn: MainDbConn) -> Result<Markup, Error> {
    let leagues = models::League::get_all(&db_conn)?;
    Ok(page(html! {
        (admin_header());
        h2 {"Servers"}
        @for i in leagues.iter() {
            form method="post" action=(uri!(server_save)) {
                div class="form-row" {
                    div class="col-2" {
                        "League " (i.id)
                    }
                    (form_field_hidden("league_id", &i.id.to_string()));
                    @match i.get_server(&db_conn)? {
                        Some(s) => {
                            (form_field_hidden("id", &s.id.to_string()));
                            div class="col-4" {
                                (form_field("name", "Name", &s.name));
                            }
                            div class="col-4" {
                                (form_field("address", "Address", &s.address));
                            }
                        },
                        None => {
                            div class="col-4" {
                                (form_field("name", "Name", ""));
                            }
                            div class="col-4" {
                                (form_field("address", "Address", ""));
                            }
                        }
                    }
                    div class="col-2" {
                        button class="btn btn-primary" {"Save"}
                    }
                }
            }
        }
    }))
}

#[post("/admin/servers/save", data = "<server>")]
pub fn server_save(
    server: Result<Form<forms::Server>, FormError>,
    db_conn: MainDbConn,
) -> Result<Redirect, Error> {
    match server {
        Ok(s) => {
            s.save(&db_conn)?;
            Ok(Redirect::to(uri!(server_list)))
        }
        Err(e) => {
            println!("{:?}", e);
            bail!("Error saving server")
        }
    }
}
