use crate::models;
use crate::schema::*;
use diesel::prelude::*;
use failure::Error;
use rocket_codegen::FromForm;

#[derive(FromForm, Debug, Insertable, AsChangeset)]
#[table_name = "player"]
pub struct Player {
    pub country: String,
    pub nickname: String,
    pub seeding: i32,
}

impl Player {
    pub fn save_new(&self, conn: &diesel::PgConnection) -> Result<(), Error> {
        models::Player::shift_seeds_up(self.seeding, conn)?;
        diesel::insert_into(player::table)
            .values(self)
            .execute(conn)?;
        models::Player::regen_leagues(conn)?;
        Ok(())
    }

    pub fn save_existing(&self, id: i32, conn: &diesel::PgConnection) -> Result<(), Error> {
        models::Player::shift_seeds_up(self.seeding, conn)?;
        diesel::update(player::table.filter(player::id.eq(id)))
            .set(self)
            .execute(conn)?;
        models::Player::fix_seeds(conn)?;
        models::Player::regen_leagues(conn)?;
        Ok(())
    }

    pub fn delete(id: i32, conn: &diesel::PgConnection) -> Result<(), Error> {
        diesel::delete(player::table.filter(player::id.eq(id))).execute(conn)?;
        models::Player::fix_seeds(conn)?;
        models::Player::regen_leagues(conn)?;
        Ok(())
    }
}

#[derive(FromForm, Debug, AsChangeset)]
#[table_name = "player"]
pub struct PlayerRestricted {
    pub country: String,
    pub nickname: String,
}

impl PlayerRestricted {
    pub fn save(&self, id: i32, conn: &diesel::PgConnection) -> Result<(), Error> {
        diesel::update(player::table.filter(player::id.eq(id)))
            .set(self)
            .execute(conn)?;
        Ok(())
    }
}

#[derive(FromForm, Debug, Insertable, AsChangeset)]
#[table_name = "map"]
pub struct Map {
    pub name: String,
}

impl Map {
    pub fn save_new(&self, conn: &diesel::PgConnection) -> Result<(), Error> {
        diesel::insert_into(map::table).values(self).execute(conn)?;
        Ok(())
    }

    pub fn save_existing(&self, id: i32, conn: &diesel::PgConnection) -> Result<(), Error> {
        diesel::update(map::table.filter(map::id.eq(id)))
            .set(self)
            .execute(conn)?;
        Ok(())
    }

    pub fn delete(id: i32, conn: &diesel::PgConnection) -> Result<(), Error> {
        diesel::delete(map::table.filter(map::id.eq(id))).execute(conn)?;
        Ok(())
    }
}

#[derive(FromForm, Debug, Insertable, AsChangeset)]
#[table_name = "game_score"]
#[changeset_options(treat_none_as_null = "true")]
pub struct GameScore {
    pub game_id: i32,
    pub player_id: i32,
    pub score: i32,
    pub tiebreaker: Option<i32>,
}

impl GameScore {
    pub fn save(&self, conn: &diesel::PgConnection) -> Result<(), Error> {
        let game = models::Game::get(self.game_id, conn)?;
        let maybe_score = game.get_score_for_player(self.player_id, conn)?;
        match maybe_score {
            Some(score) => {
                diesel::update(&score).set(self).execute(conn)?;
            }
            None => {
                diesel::insert_into(game_score::table)
                    .values(self)
                    .execute(conn)?;
            }
        }
        Ok(())
    }
}

#[derive(FromForm, Debug, Insertable, AsChangeset)]
#[table_name = "page"]
pub struct Page {
    pub id: Option<i32>,
    pub slug: String,
    pub content: String,
}

impl Page {
    pub fn save(&self, conn: &diesel::PgConnection) -> Result<(), Error> {
        match self.id {
            Some(id) => {
                diesel::update(page::table)
                    .filter(page::id.eq(id))
                    .set(self)
                    .execute(conn)?;
            }
            None => {
                diesel::insert_into(page::table)
                    .values(self)
                    .execute(conn)?;
            }
        }
        Ok(())
    }
}

#[derive(FromForm, Debug, Insertable, AsChangeset)]
#[table_name = "server"]
pub struct Server {
    pub id: Option<i32>,
    pub league_id: i32,
    pub name: String,
    pub address: String,
}

impl Server {
    pub fn save(&self, conn: &diesel::PgConnection) -> Result<(), Error> {
        match self.id {
            Some(id) => {
                diesel::update(server::table)
                    .filter(server::id.eq(id))
                    .set(self)
                    .execute(conn)?;
            }
            None => {
                diesel::insert_into(server::table)
                    .values(self)
                    .execute(conn)?;
            }
        }
        Ok(())
    }
}
