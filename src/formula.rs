use rust_decimal::Decimal;

// Kudos to GATTS, Packer and other BOTs for helping with polishing this

const BASE_FACTOR: f64 = 1.15;
const SECONDARY_FACTOR: f64 = 1.5;

pub fn compute_score(score: i32, league: i32, total_leagues: i32) -> Decimal {
    let tot = total_leagues as f64;
    let cur = league as f64;
    let pow = (SECONDARY_FACTOR * tot).ln() / BASE_FACTOR.ln() * (tot - cur) / (tot - 1.);
    let mul = BASE_FACTOR.powf(pow);
    let final_score = (score as f64) * mul;
    Decimal::new((final_score * 1000.).round() as i64, 3)
}
