use crate::constants::*;
use crate::formula::compute_score;
use crate::markup::*;
use crate::models;
use crate::models::{get_tournament_state, TournamentState};
use crate::MainDbConn;
use failure::{bail, Error};
use maud::{html, Markup, PreEscaped};
use rocket::{get, uri};
use rust_decimal::Decimal;
use std::cmp::Ordering;
use std::collections::HashMap;

fn header() -> Markup {
    html! {
        h1 class="text-center" { "XQC #90. InstaGib FFA Cup" }
        hr;
        div class="row" {
            div class="col-12 text-center" {
                a href="/" { "Home" }
                span { " | " }
                a href=(uri!(public_page: slug="rules")) { "Rules"}
                span { " | " }
                a href=(uri!(servers)) { "Servers"}
            }
        }
        hr;
    }
}

#[derive(Debug, PartialEq, Eq)]
struct Score {
    // (score, league_id, computed_score)
    rounds: [Option<(i32, i32, Decimal)>; (NUMBER_OF_ROUNDS as usize)],
    total_score: Decimal,
}

impl Ord for Score {
    fn cmp(&self, other: &Score) -> Ordering {
        self.total_score.cmp(&other.total_score)
    }
}

impl PartialOrd for Score {
    fn partial_cmp(&self, other: &Score) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Default for Score {
    fn default() -> Score {
        Score {
            rounds: [None; (NUMBER_OF_ROUNDS as usize)],
            total_score: Decimal::new(0, 0),
        }
    }
}

fn scores_table(db_conn: &diesel::PgConnection) -> Result<Markup, Error> {
    let total_leagues = models::League::get_all(db_conn)?.len() as i32;
    let totals = models::GameScore::get_totals(db_conn)?;
    let mut scores: HashMap<models::Player, Score> = HashMap::new();
    for (game_score, game, player) in totals {
        if (game.round_id < 1) || (game.round_id > NUMBER_OF_ROUNDS) {
            bail!("round id out of bounds {}", game.round_id)
        }
        let mut score = scores.entry(player).or_insert(Score::default());
        let points = compute_score(game_score.score, game.league_id, total_leagues);
        score.rounds[(game.round_id - 1) as usize] =
            Some((game_score.score, game.league_id, points));
        score.total_score += points;
    }
    let mut sorted_scores: Vec<(models::Player, Score)> = scores.drain().collect();
    sorted_scores.sort_by(|x, y| y.1.cmp(&x.1));
    Ok(html! {
        table class="table table-bordered table-striped" {
            thead {
                tr {
                    th rowspan="2" { "" }
                    th rowspan="2" { "Player" }
                    @for i in 0..NUMBER_OF_ROUNDS {
                        th colspan="3" { (format!("Round {}", i+1)) }
                    }
                    th rowspan="3" { "Total" }
                }
                tr {
                    @for _ in 0..NUMBER_OF_ROUNDS {
                        th { "L" }
                        th { "F"}
                        th { "S" }
                    }
                }
            }
            tbody {
                @for (ix, score) in sorted_scores.iter().enumerate() {
                    tr {
                        td {
                            (format!("{}", ix + 1)) "."
                        }
                        td {
                            (player(&score.0))
                        }
                        @for maybe_r in score.1.rounds.iter() {
                            @if let Some(r) = maybe_r {
                                td {(r.1)}
                                td {(r.0)}
                                td {(r.2.round())}
                            }
                            @else {
                                td {}
                                td {}
                                td {}
                            }
                        }
                        td {strong { (score.1.total_score) }}
                    }
                }
            }
        }
    })
}

#[get("/page/<slug>")]
pub fn public_page(slug: String, db_conn: MainDbConn) -> Result<Markup, Error> {
    let maybe_page = models::Page::by_slug(&slug, &db_conn)?;
    match maybe_page {
        Some(p) => Ok(page_fluid(html! {
            (header());
            div class="container" {
                div class="row" {
                    div class="col-12" {
                        (PreEscaped(p.content))
                    }
                }
            }
        })),
        None => bail!("No such page"),
    }
}

#[get("/servers")]
pub fn servers(db_conn: MainDbConn) -> Result<Markup, Error> {
    let leagues = models::League::get_all(&db_conn)?;
    Ok(page_fluid(html! {
        (header());
        div class="container" {
            div class="row" {
                div class="col-12" {
                    ul {
                        @for i in leagues.iter() {
                            li {
                                @match i.get_server(&db_conn)? {
                                    Some(s) => code {(s.name) ": " (s.address)},
                                    None => code {"TBD"}
                                }
                            }
                        }
                    }
                }
            }
        }
    }))
}

#[get("/")]
pub fn index(db_conn: MainDbConn) -> Result<Markup, Error> {
    let state = get_tournament_state(&db_conn)?;
    match state {
        TournamentState::Seeding => Ok(page_fluid(html! {
            (header())
                div class="row mb-3" {
                    div class="col" {
                        h3 {"Tournament not yet started."}
                        p { a href="https://forums.xonotic.org/showthread.php?tid=7978"
                            {"SATURDAY, March 16th. 19:00 CET (Check-in 18:00 CET)" }}
                    }
                }

        })),
        TournamentState::RoundActive(round) => {
            let map_name = &round.get_map(&db_conn)?.name;
            let players = models::Player::by_league(round.id, &db_conn)?;
            println!("Players {:?}", players);

            Ok(page_fluid(html! {
                (header())
                div class="row mb-3" {
                    div class="col-7" {
                        h3 { "Current leagues" }
                        @for (league, league_players) in players.iter() {
                            h4 { "League " (league) }
                            @match models::Server::get_for_league(*league, &db_conn)? {
                                Some(s) => p { code { "Server: " (s.name) " - " (s.address) } },
                                None => {}
                            }
                            ul class="league-list" {
                                @for (p, prev_round) in league_players {
                                    li {
                                        (player(p))
                                            @if let Some((_, game)) = prev_round {
                                                @if let Some(current_league) = p.league_id {
                                                    @if game.league_id < current_league {
                                                        span class="relegated ml-2" {
                                                            i class="far fa-arrow-alt-circle-down" {""}
                                                        }
                                                    }
                                                    @else if game.league_id > current_league {
                                                        span class="promoted ml-2" {
                                                            i class="far fa-arrow-alt-circle-up" {""}
                                                        }
                                                    }
                                                }
                                            }
                                    }
                                }
                            }
                        }
                    }
                    div class="col-5" {
                        h3 { "Current map" }
                        img class="img-fluid" src={ "/static/img/" (map_name) ".jpg" };
                    }
                }
                div class="row" {
                    h3 { "Standings" }
                    (scores_table(&db_conn)?)
                }
            }))
        }
        TournamentState::TournamentFinished(_round) => Ok(page_fluid(html! {
            (header());
            p { "See you next time!" }
            div class="row" {
                h3 { "Standings" }
                (scores_table(&db_conn)?)
            }
        })),
    }
}
