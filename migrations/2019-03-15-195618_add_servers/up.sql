CREATE TABLE server (
       id serial primary key,
       league_id integer not null references league(id),
       name character varying(256) not null,
       address character varying(256) not null
);
