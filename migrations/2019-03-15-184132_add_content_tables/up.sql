CREATE TABLE page (
       id serial primary key,
       slug character varying(512) NOT NULL,
       content text not null
);

CREATE UNIQUE INDEX page_slug ON page USING btree (slug);
