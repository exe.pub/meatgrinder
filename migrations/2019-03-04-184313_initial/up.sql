CREATE TABLE league (
       id integer PRIMARY KEY
);

CREATE TABLE player (
       id SERIAL PRIMARY KEY,
       nickname character varying(512) NOT NULL,
       league_id integer NOT NULL REFERENCES league(id) ON DELETE RESTRICT
);


-- CREATE TABLE player_key (
--        crypto_idfp varying(255) NOT NULL PRIMARY KEY,
--        player_id integer NOT NULL REFERENCES player(id) ON DELETE CASCADE
-- );

CREATE TABLE player_sub (
       id SERIAL PRIMARY KEY,
       player_id integer NOT NULL REFERENCES player(id),
       nickname character varying(512) NOT NULL,
       is_active boolean
);


CREATE TABLE map (
       id SERIAL PRIMARY KEY,
       name character varying(128)
);

CREATE INDEX map_name ON map USING btree (name);

CREATE TABLE round (
       id integer NOT NULL PRIMARY KEY,
       map_id integer NOT NULL REFERENCES map(id) ON DELETE RESTRICT
);


CREATE TABLE game (
       id serial primary key,
       round_id integer NOT NULL REFERENCES round(id) ON DELETE RESTRICT,
       league_id integer NOT NULL REFERENCES league(id) ON DELETE RESTRICT
);

CREATE TABLE game_score (
       id serial primary key,
       game_id integer NOT NULL REFERENCES game(id) ON DELETE RESTRICT,
       player_id integer NOT NULL REFERENCES player(id) ON DELETE RESTRICT,
       kills integer NOT NULL,
       deaths integer NOT NULL,
       suicides integer NOT NULL,
       time_to_reach_kills integer
);
