ALTER TABLE game_score DROP COLUMN kills;
ALTER TABLE game_score DROP COLUMN deaths;
ALTER TABLE game_score DROP COLUMN suicides;
ALTER TABLE game_score DROP COLUMN time_to_reach_kills;

ALTER TABLE game_score ADD COLUMN score integer NOT NULL default 0;
ALTER TABLE game_score ADD COLUMN tiebreaker integer; 
