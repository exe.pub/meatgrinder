ALTER TABLE game_score ADD COLUMN kills integer NOT NULL default 0;
ALTER TABLE game_score ADD COLUMN deaths integer not NULL default 0;
ALTER TABLE game_score ADD COLUMN suicides integer not NULL default 0;
ALTER TABLE game_score ADD COLUMN time_to_reach_kills integer;

ALTER TABLE game_score DROP COLUMN score;
ALTER TABLE game_score DROP COLUMN tiebreaker;
